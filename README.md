
 Set Rover postion and location 

setPostionAndLocation(x, y, cardinalPoint);
x & y --> co-ordinates
cardinalPoint --> N E S W

Example Position 

x-> 0 
y-> 0
cardinalPoint->N

setPostionAndLocation(0, 0, N)

To control the rover -->

L --> spin 90 degree left --> no change in current spot
R --> spin  90 degree right --> no change in current spot
M --> move forward one grid point


Input -->  5 lines in the example

1 --> upper right corner and lower left 0 0
2 lines for rover

1st line --> setPostionAndLocation(1, 2, N)
2nd line --> setExploreDirection(LMLMLMLMM)

