class Utility {
    bill: Bill
    name: String
    constructor(bill: Bill, name: String) {
        this.bill = bill;
        this.name = name;
    }
}
class Bill {
    rate: number
    consumption: number
    additonal: number
    constructor(rate: number, consumption: number, additonal?: number) {
        this.rate = rate;
        this.consumption = consumption;
        this.additonal = additonal || 0;
    }
    calaculateTotal() {
        let amount = Math.round(this.consumption * this.rate) + this.additonal;
        return amount;
    }
}
class TaxableBill extends Bill {
    tax: number
    constructor(rate: number, consumption: number, additonal?: number, tax?: number) {
        super(rate, consumption, additonal);
        this.tax = tax || 0;
    }
    calaculateTotal() {
        let value = super.calaculateTotal();
        let result = value + (value * this.tax);
        return result;
    }
}

let house = new Utility(new TaxableBill(10, 650), "house");
let water = new Utility(new TaxableBill(0.001, 150000), "water");
let electricity = new Utility(new TaxableBill(10, 200, 0, 0.18), "electricity");
let phone = new Utility(new TaxableBill(0.01, 50000, 100, 0.5), "phone");
let gas = new Utility(new TaxableBill(15, 15), "gas");
let houseBillAmount = house.bill.calaculateTotal();
let waterBillAmount = water.bill.calaculateTotal();
let electricityBillAmount = electricity.bill.calaculateTotal();
let phoneBillAmount = phone.bill.calaculateTotal();
let GasBillAmount = gas.bill.calaculateTotal();

console.log("Total spending for this month: INR ", (waterBillAmount + electricityBillAmount + phoneBillAmount + GasBillAmount));
console.log('Water Bill Amount', waterBillAmount);
console.log('Electricity Bill Amount', electricityBillAmount);
console.log('Phone Bill Amount', phoneBillAmount);
console.log('Gas Bill Amount', GasBillAmount);
console.log("Other annual expenses:");
console.log("House Bill: INR ", houseBillAmount);