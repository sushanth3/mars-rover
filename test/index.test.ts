import { Rover } from "../src/rover";
import { RoverControl } from "../src/rover-movements";
import { Series } from "../src/series";
import { Cordinates } from "../src/upper-coordinates";

const request = require('supertest');

describe('Mars-Rover', () => {

    it('check spin left', () => {
        let rover = new Rover(1, 2, "N");
        let cmd = new RoverControl();
        let result = cmd.spinLeft(rover.direction);
        rover.direction = result;
        expect(rover.direction).toBe("W");
    });
    it('check spin right', () => {
        let rover = new Rover(1, 2, "N");
        let cmd = new RoverControl();
        let result = cmd.spinRight(rover.direction);
        rover.direction = result;
        expect(rover.direction).toBe("E");
    });
    it('check move forward', () => {
        let rover = new Rover(1, 2, "N");
        let cmd = new RoverControl();
        let result = cmd.moveForward(rover.x, rover.y, rover.direction);
        rover.x = result.x;
        rover.y = result.y;
        rover.direction = result.direction;
        expect(rover.x).toBe(1);
        expect(rover.y).toBe(3);
        expect(rover.direction).toBe("N");
    });
    it('check valid result for instruction 1', () => {
        let rover = new Rover(1, 2, "N");
        let instructions = new Series("LMLMLMLMM");

        let cmd = new RoverControl();

        for (let i = 0; i < instructions.x.length; i++) {
            let singleCommand = instructions.x.charAt(i);
            switch (singleCommand) {
                case "L":
                    let result = cmd.spinLeft(rover.direction);
                    rover.direction = result;
                    break;
                case "R":
                    let result2 = cmd.spinRight(rover.direction);
                    rover.direction = result2;
                case "M":
                    let result3 = cmd.moveForward(rover.x, rover.y, rover.direction);
                    rover.x = result3.x;
                    rover.y = result3.y;
                    rover.direction = result3.direction;
                default:
                    break;
            }
        }
        expect(rover.x).toBe(1);
        expect(rover.y).toBe(3);
        expect(rover.direction).toBe("N");
    });

    it('check valid result for instruction 2', () => {
        let rover = new Rover(3, 3, "E");
        let instructions = new Series("MMRMMRMRRM");

        let cmd = new RoverControl();

        for (let i = 0; i < instructions.x.length; i++) {
            let singleCommand = instructions.x.charAt(i);
            switch (singleCommand) {
                case "L":
                    let result = cmd.spinLeft(rover.direction);
                    rover.direction = result;
                    break;
                case "R":
                    let result2 = cmd.spinRight(rover.direction);
                    rover.direction = result2;
                case "M":
                    let result3 = cmd.moveForward(rover.x, rover.y, rover.direction);
                    rover.x = result3.x;
                    rover.y = result3.y;
                    rover.direction = result3.direction;
                default:
                    break;
            }
        }
        expect(rover.x).toBe(5);
        expect(rover.y).toBe(1);
        expect(rover.direction).toBe("E");
    });
});