import { Rover } from "./rover";
import { RoverControl } from "./rover-movements";
import { Series } from "./series";
import { Cordinates } from "./upper-coordinates";

let cordinates = new Cordinates(5, 5);
console.log("cordinates", cordinates);

let rover = new Rover(1, 2, "N");
console.log("rover", rover.x, rover.y, rover.direction);

let instructions = new Series("LMLMLMLMM");

let cmd = new RoverControl();
for (let i = 0; i < instructions.x.length; i++) {
    let singleCommand = instructions.x.charAt(i);
    switch (singleCommand) {
        case "L":
            let result = cmd.spinLeft(rover.direction);
            rover.direction = result;
            break;
        case "R":
            let result2 = cmd.spinRight(rover.direction);
            rover.direction = result2;
        case "M":
            let result3 = cmd.moveForward(rover.x, rover.y, rover.direction);
            rover.x = result3.x;
            rover.y = result3.y;
            rover.direction = result3.direction;
        default:
            break;
    }
}

console.log("Final Result", rover.x, rover.y, rover.direction);