export class Rover {
    x: Number
    y: Number
    direction: String
    constructor(x: Number, y: Number, direction: String) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }
}
