
export class RoverControl {

    public spinLeft(direction: String) {
        let value = "";
        if (direction === "N")
            value = "W";
        else if (direction === "W")
            value = "S";
        else if (direction === "S")
            value = "E";
        else if (direction === "E")
            value = "N";
        else
            value = "";
        return value;
    };

    public spinRight(direction: String) {
        let value = "";
        if (direction === "N")
            value = "E";
        else if (direction === "E")
            value = "S";
        else if (direction === "S")
            value = "W";
        else if (direction === "W")
            value = "N";
        else
            value = "";
        return value;
    };

    public moveForward(x: any, y: any, direction: String) {
        if (direction === "N")
            y += 1;
        else if (direction === "E")
            x += 1;
        else if (direction === "S")
            y -= 1;
        else if (direction === "W")
            x -= 1;
        else
            console.log("Error");
        return { direction, x, y };
    };

}